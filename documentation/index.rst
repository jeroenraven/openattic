|oA| |version| Documentation
############################

The times when storage was considered a server-based resource and every system
needed to have its own hard drives are long gone. In modern data centers central
storage systems have become ubiquitous for obvious reasons. Centrally managed
storage increases flexibility and reduces the cost for unused storage reserves.
With the introduction of a cluster or virtualization solution shared storage
becomes a necessity.

This mission-critical part of IT used to be dominated by proprietary offerings.
Even though mature open source projects may now meet practically every
requirement of a modern storage system, managing and using these tools is often
quite complex and is mostly done decentrally.

|oA| is a full-fledged central storage management system. Hardware resources
can be managed, logical storage areas can be shared and distributed and data
can be stored more efficiently and less expensively than ever before – and you
can control everything from a central management interface. It is no longer
necessary to be intimately familiar with the inner workings of the individual
storage tools. Any task can be carried out by either using |oA|'s intuitive
web interface or via the REST API.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

.. toctree::
  :maxdepth: 2

  trademarks
  install_guides/index
  gui_docs/index
  developer_docs/index

.. seealso:: This documentation is also available `as a PDF file. <openATTIC.pdf>`_

Indices and Tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
