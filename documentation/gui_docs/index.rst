.. _gui_docs_index:

User Manual
###########

This section covers the |oA| web user interface (GUI), focusing on storage
tasks like adding volumes and shares, system management tasks like the
configuration of users and API credentials, and the integrated monitoring
system.

.. toctree::
  :maxdepth: 2

  admin_guide
  taskqueue
