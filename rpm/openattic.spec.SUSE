#
# spec file for package openattic
#
# Copyright (c) 2016-2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/

Name: openattic
Version: 3.1.2
Release: 0
Summary: Comprehensive Storage Management System
Group: System Environment/Libraries
License: GPL-2.0
URL: http://www.openattic.org
BuildArch: noarch
Source:	%{name}-%{version}.tar.bz2
BuildRequires: -post-build-checks
%if 0%{?suse_version} >= 1210
BuildRequires: systemd-rpm-macros
%endif
%if 0%{?suse_version} >= 1020
BuildRequires:  fdupes
%endif
BuildRequires: apache2
BuildRequires: lvm2
BuildRequires: icinga
BuildRequires: nagios-rpm-macros
# BuildRequires: policycoreutils
# BuildRequires: policycoreutils-python
BuildRequires: postgresql
BuildRequires: postgresql-server
BuildRequires: pnp4nagios
BuildRequires: pnp4nagios-icinga
BuildRequires: python
BuildRequires: rsync
Requires:	%{name}-module-icinga
Requires:	%{name}-pgsql

# These subpackages have been removed in 2.0.19 (OP#1968)
Obsoletes: %{name}-module-ipmi <= 2.0.18
Obsoletes: %{name}-module-mdraid <= 2.0.18
Obsoletes: %{name}-module-twraid <= 2.0.18

# These subpackages have been removed in 3.0.0 (OP#2127)
Obsoletes: %{name}-module-btrfs <= 3.0.0
Obsoletes: %{name}-module-cron <= 3.0.0
Obsoletes: %{name}-module-drbd <= 3.0.0
Obsoletes: %{name}-module-http <= 3.0.0
Obsoletes: %{name}-module-lio <= 3.0.0
Obsoletes: %{name}-module-lvm <= 3.0.0
Obsoletes: %{name}-module-mailaliases <= 3.0.0
Obsoletes: %{name}-module-nfs <= 3.0.0
Obsoletes: %{name}-module-samba <= 3.0.0
Obsoletes: %{name}-module-volumes <= 3.0.0
Obsoletes: %{name}-module-zfs <= 3.0.0

# OpenATTIC and Crowbar's apache configurations conflict with each other,
# and in any case we (SUSE) don't support installing openATTIC on a Crowbar
# admin node.
Conflicts:  crowbar
Recommends: logrotate
Recommends: %{name}-gui
Recommends: %{name}-module-ceph
Recommends: %{name}-module-ceph-deployment

%description
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

It comes with an extensible API focused on modularity, so you can tailor
your installation exactly to your needs and embed openATTIC in your existing
data center infrastructure.

This metapackage installs the most common set of openATTIC modules along
with the basic requirements.

Upstream URL: http://www.openattic.org

%package base
%{?systemd_requires}
Requires:	bridge-utils
Requires:	bzip2
Requires:	dbus-1
Requires:	python-django-filter
Requires:	python-djangorestframework = 2.4.4
Requires:	python-djangorestframework-bulk
Requires:	python-M2Crypto
Requires:	memcached
Requires:	apache2-mod_wsgi
Requires:	ntp
Requires:	python-numpy
Requires:	policycoreutils
Requires:	python-gobject2
Requires:	dbus-1-python
Requires:	python-configobj
Requires:	python-django = 1.6.11
Requires:	python-imaging
Requires:	python-m2ext
Requires:	python-memcached
Requires:	python-netaddr
Requires:	python-netifaces
Requires:	python-pam
Requires:	python-psycopg2
Requires:	python-pyudev
Requires:	python-requests
Requires:	python-simplejson
Requires:	udisks2
Requires:	vlan
Requires:	wget
Requires:	xfsprogs
Requires:	logrotate
Requires(pre): shadow
Summary: Basic requirements for openATTIC

%description base
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

This package installs the basic framework for openATTIC, which consists
of the RPC and System daemons. You will not be able to manage any storage
using *just* this package, but the other packages require this one to be
available.

%package gui
Requires: %{name}-base = %{version}
Requires: policycoreutils
Requires: policycoreutils-python
Requires: selinux-tools
Summary: The openATTIC Web Interface

%description gui
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

This package includes the Web UI based on AngularJS/Bootstrap.

%package module-ceph
Requires: ceph-common >= 10.0.0
Requires: %{name}-base = %{version}
Requires: %{name}-module-icinga = %{version}
Requires: python-requests-aws
Summary: Ceph module for openATTIC

%description module-ceph
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

This package includes support for Ceph, a distributed storage system
designed to provide excellent performance, reliability, and scalability.

%package module-ceph-deployment
Requires: ceph-common >= 10.0.0
Requires: %{name}-module-ceph
Summary: Ceph deployment and management module for openATTIC

%description module-ceph-deployment
openATTIC is a storage management system based upon Open Source tools with a
comprehensive user interface that allows you to create, share and backup storage
space on demand.

This package includes deployment and remote management support for Ceph, a
distributed storage system designed to provide excellent performance,
reliability, and scalability. It is based on the "DeepSea" collection of Salt
files (https://github.com/SUSE/DeepSea).

%package  module-icinga
Requires:	icinga
Requires:	monitoring-plugins-common
Requires: %{name}-base = %{version}
Requires:	pnp4nagios
Requires: monitoring-plugins-http
Requires: monitoring-plugins-swap
Requires: monitoring-plugins-ssh
Requires: monitoring-plugins-ping
Requires: monitoring-plugins-disk
Requires: monitoring-plugins-users
Requires: monitoring-plugins-procs
Requires: monitoring-plugins-load
Requires: monitoring-plugins-tcp

Summary: Nagios module for openATTIC

%description module-icinga
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

Nagios is a widely used system monitoring solution. This package installs a
module which automatically configures service checks for your configured
volumes and shares, measures performance data, and provides you with an
intuitive user interface to view the graphs.

This package also contains the Nagios check plugin 'check_openattic_systemd'.

%package pgsql
Requires: postgresql
Requires:	postgresql-server
Requires(pre):	%{name}-base = %{version}
Summary: PGSQL database for openATTIC

%description pgsql
openATTIC is a storage management system based upon Open Source tools with
a comprehensive user interface that allows you to create, share and backup
storage space on demand.

This package configures the PostgreSQL database for openATTIC.

%prep
%setup -q

%build

%install

# Build up target directory structure
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/openattic-gui
mkdir -p %{buildroot}%{_localstatedir}/lib/%{name}/static
mkdir -p %{buildroot}%{_localstatedir}/log/%{name}
mkdir -p %{buildroot}%{_localstatedir}/lock/%{name}
mkdir -p %{buildroot}/srv/www/htdocs/
mkdir -p %{buildroot}%{_mandir}/man1/
mkdir -p %{buildroot}%{nagios_plugindir}
mkdir -p %{buildroot}%{icinga_sysconfdir}/objects/
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_sysconfdir}/apache2/conf.d/
mkdir -p %{buildroot}%{_sysconfdir}/dbus-1/system.d/
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
mkdir -p %{buildroot}%{_sysconfdir}/modprobe.d/
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/databases
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_prefix}/lib/tmpfiles.d/
%if 0%{?suse_version}
mkdir -p %{buildroot}/var/adm/fillup-templates
%else
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
%endif

# Install Backend and binaries
rsync -aAX backend/ %{buildroot}%{_datadir}/%{name}
install -m 644 version.txt %{buildroot}%{_datadir}/%{name}
rm -rf %{buildroot}%{_datadir}/%{name}/.style.yapf
rm -rf %{buildroot}%{_datadir}/%{name}/.pep8
install -m 755 bin/oaconfig   %{buildroot}%{_sbindir}
install -d 755 %{buildroot}%{_libdir}/nagios/plugins
#install -m 755 backend/nagios/plugins/check_cephcluster %{buildroot}%{_libdir}/nagios/plugins
#install -m 755 backend/nagios/plugins/check_cephpool %{buildroot}%{_libdir}/nagios/plugins
#install -m 755 backend/nagios/plugins/check_cephrbd %{buildroot}%{_libdir}/nagios/plugins

%py_compile %{buildroot}%{_datadir}/%{name}

# Install Web UI
rsync -aAX webui/dist/ %{buildroot}%{_datadir}/openattic-gui/
sed -i -e 's/^ANGULAR_LOGIN.*$/ANGULAR_LOGIN = False/g' %{buildroot}%{_datadir}/%{name}/settings.py

# Install HTML redirect
install -m 644 webui/redirect.html %{buildroot}/srv/www/htdocs/index.html

%if 0%{?suse_version}
install -m 644 rpm/sysconfig/%{name}.SUSE %{buildroot}/var/adm/fillup-templates/sysconfig.%{name}
%else
install -m 644 rpm/sysconfig/%{name}.RedHat %{buildroot}%{_sysconfdir}/sysconfig/%{name}
%endif

# Install db file
install -m 640 etc/openattic/database.ini %{buildroot}%{_sysconfdir}/%{name}/

# configure dbus
install -m 644 etc/dbus-1/system.d/%{name}.conf.SUSE %{buildroot}%{_sysconfdir}/dbus-1/system.d/%{name}.conf

install -m 644 etc/logrotate.d/%{name} %{buildroot}%{_sysconfdir}/logrotate.d/
touch %{buildroot}%{_localstatedir}/log/%{name}/%{name}.log

# install man pages
install -m 644 man/*.1 %{buildroot}%{_mandir}/man1/
gzip %{buildroot}%{_mandir}/man1/*.1

#configure nagios
install -m 644 etc/nagios-plugins/config/%{name}.cfg %{buildroot}%{icinga_sysconfdir}/objects/%{name}_plugins.cfg
echo >> %{buildroot}%{icinga_sysconfdir}/objects/%{name}_plugins.cfg
cat etc/nagios-plugins/config/%{name}-ceph.cfg >> %{buildroot}%{icinga_sysconfdir}/objects/%{name}_plugins.cfg
install -m 644 etc/nagios3/conf.d/%{name}_*.cfg      %{buildroot}%{icinga_sysconfdir}/objects/

for NAGPLUGIN in `ls -1 %{buildroot}%{_datadir}/%{name}/nagios/plugins/`; do
    ln -s "%{_datadir}/%{name}/nagios/plugins/$NAGPLUGIN" "%{buildroot}%{nagios_plugindir}/$NAGPLUGIN"
done

install -m 444 etc/systemd/%{name}-systemd.service.SUSE %{buildroot}%{_unitdir}/%{name}-systemd.service
ln -s %{_sbindir}/service %{buildroot}%{_sbindir}/rcopenattic-systemd
install -m 644 etc/tmpfiles.d/%{name}.conf %{buildroot}%{_prefix}/lib/tmpfiles.d/

# openATTIC httpd config
install -m 644 etc/apache2/conf-available/%{name}.conf         %{buildroot}%{_sysconfdir}/apache2/conf.d/

%if 0%{?suse_version} >= 1020
%fdupes %{buildroot}
%endif

%pre base
# create openattic user/group  if it does not exist
if getent group openattic > /dev/null ; then
  echo "openattic group already exists"
else
  groupadd -r openattic 2>/dev/null || :
  usermod -a --groups openattic wwwrun 2>/dev/null || :
  usermod -a --groups openattic %{nagios_user} 2>/dev/null || :
  usermod -a -G openattic icinga || :
fi

if getent passwd openattic > /dev/null ; then
  echo "openattic user already exists"
else
  useradd -r -g openattic -d /var/lib/openattic -s /bin/bash -c "openATTIC System User" openattic 2>/dev/null || :
  usermod -a --groups %{nagios_group}, %{nagios_command_group},nagcmd,www openattic 2>/dev/null || :
fi
%service_add_pre %{name}-systemd.service
exit 0

%post base
%service_add_post %{name}-systemd.service
%fillup_and_insserv
systemd-tmpfiles --create %{_prefix}/lib/tmpfiles.d/%{name}.conf

%posttrans
# The setup of apache2 may not be finished at the %post stage due to
# scriptlets included in apache2's own posttrans. So we need to move
# the attempt to start it here:
oaconfig install || oaconfig install --allow-broken-hostname
systemctl enable apache2
systemctl start apache2

%preun base
systemd-tmpfiles --remove %{_prefix}/lib/tmpfiles.d/%{name}.conf
%service_del_preun %{name}-systemd.service

%postun base
%service_del_postun %{name}-systemd.service
systemctl try-restart apache2

%post gui
# semanage fcontext -a -t httpd_sys_rw_content_t "/usr/share/openattic-gui(/.*)?"
# restorecon -vvR
systemctl try-restart apache2

%postun gui
# semanage fcontext -d -t httpd_sys_rw_content_t "/usr/share/openattic-gui(/.*)?"
# restorecon -vvR
systemctl try-restart apache2

%post pgsql
# Configure Postgres DB
systemctl enable postgresql
systemctl start postgresql

%postun pgsql
if [ $1 -eq 0 ] ; then
  echo "Note: removing this package does not delete the"
  echo "corresponding PostgreSQL database by default."
  echo "If you want to drop the openATTIC database and"
  echo "database user, run the following commands as root:"
  echo ""
  echo "su - postgres -c psql"
  echo "postgres=# drop database openattic;"
  echo "postgres=# drop user openattic;"
  echo "postgres=# \q"
  echo ""
fi

%files
%defattr(-,root,root,-)
%doc CHANGELOG CONTRIBUTING.rst COPYING README.rst

%files base
%defattr(-,openattic,openattic,-)
%dir %{_localstatedir}/lib/%{name}
%attr(0775,-,-) %dir %{_localstatedir}/log/%{name}
%attr(660,-,-) %{_localstatedir}/log/%{name}/%{name}.log
%ghost %dir %{_localstatedir}/lock/%{name}
%defattr(-,root,root,-)
%{_sbindir}/oaconfig
%config %{_sysconfdir}/dbus-1/system.d/%{name}.conf
%{_unitdir}/%{name}-systemd.service
%{_prefix}/lib/tmpfiles.d/%{name}.conf
%{_sbindir}/rcopenattic-systemd
%config %{_sysconfdir}/apache2/conf.d/%{name}.conf
%if 0%{?suse_version}
/var/adm/fillup-templates/sysconfig.%{name}
%else
%config %{_sysconfdir}/sysconfig/openattic
%endif
%config %{_sysconfdir}/logrotate.d/%{name}
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/installed_apps.d
%dir %{_sysconfdir}/%{name}/
%doc %{_mandir}/man1/oaconfig.1.gz
%{_datadir}/%{name}/cmdlog/
%{_datadir}/%{name}/ifconfig/
%{_datadir}/%{name}/__init__.py*
%{_datadir}/%{name}/installed_apps.d/60_taskqueue
%{_datadir}/%{name}/manage.py*
%{_datadir}/%{name}/nodb/
%{_datadir}/%{name}/oa_auth.py*
%{_datadir}/%{name}/openattic.wsgi
%{_datadir}/%{name}/pamauth.py*
%{_datadir}/%{name}/processors.py*
%{_datadir}/%{name}/rest/
%{_datadir}/%{name}/rest_client.py*
%{_datadir}/%{name}/settings.py*
%{_datadir}/%{name}/deepsea.py*
%{_datadir}/%{name}/systemd/
%{_datadir}/%{name}/sysutils/
%{_datadir}/%{name}/taskqueue/
%{_datadir}/%{name}/templates/
%{_datadir}/%{name}/urls.py*
%{_datadir}/%{name}/userprefs/
%{_datadir}/%{name}/version.txt
%{_datadir}/%{name}/views.py*
%{_datadir}/%{name}/volumes/
%{_datadir}/%{name}/exception.py*
%{_datadir}/%{name}/utilities.py*

%files module-ceph
%defattr(-,root,root,-)
%{_datadir}/%{name}/installed_apps.d/60_ceph
%{_datadir}/%{name}/installed_apps.d/60_ceph_radosgw
%{_datadir}/%{name}/ceph/
%{_datadir}/%{name}/ceph_radosgw/
%{nagios_plugindir}/check_cephcluster
%{nagios_plugindir}/check_cephpool
%{nagios_plugindir}/check_cephrbd

%files module-ceph-deployment
%defattr(-,root,root,-)
%{_datadir}/%{name}/installed_apps.d/60_ceph_deployment
%{_datadir}/%{name}/ceph_deployment/
%{_datadir}/%{name}/installed_apps.d/60_ceph_iscsi
%{_datadir}/%{name}/ceph_iscsi/

%files gui
%defattr(-,root,root,-)
%{_datadir}/%{name}-gui
/srv/www/htdocs/index.html

%files module-icinga
#/etc/pnp4nagios/check_commands/check_all_disks.cfg
%defattr(-,root,root,-)
%config %{icinga_sysconfdir}/objects/openattic_plugins.cfg
%config %{icinga_sysconfdir}/objects/openattic_static.cfg
%config %{icinga_sysconfdir}/objects/openattic_contacts.cfg
%{nagios_plugindir}/check_openattic_systemd
%{nagios_plugindir}/notify_openattic
%{_datadir}/%{name}/installed_apps.d/50_nagios
%{_datadir}/%{name}/nagios
%attr(0644, -, -) %{_datadir}/%{name}/nagios/restapi.py*

%post module-icinga
chkconfig icinga on
chkconfig npcd on
systemctl start icinga.service
systemctl start npcd.service
grep -q objects/openattic_plugins.cfg$ %{icinga_sysconfdir}/icinga.cfg || \
sed -i '/^# You can specify individual object/a \
cfg_file=%{icinga_sysconfdir}/objects/openattic_plugins.cfg\
cfg_file=%{icinga_sysconfdir}/objects/openattic_static.cfg' \
        %{icinga_sysconfdir}/icinga.cfg

%files 	pgsql
%defattr(-,openattic,openattic,-)
%config(noreplace) %{_sysconfdir}/%{name}/database.ini

%changelog
* Fri Jan 08 2016 Lenz Grimmer <lenz@openattic.org> 2.0.5
- Update to version 2.0.5
- Added openattic-module-lio to the openattic meta package requirements
- Make more use of the name RPM macro when installing files
- Make sure to enable apache2 on bootup in the base postinstall
- Fixed doc file list (LICENSE was renamed to COPYING, added CONTRIBUTING.rst)
- Make sure to enable and activate rpcbind and nfsserver when the nfs module
  is installed
- Make sure to enable and activate smb and nmb when the samba module is
  installed.
- Replaced monitoring-plugins-common dependency with
  nagios-core-plugins-common, to avoid an installation conflict
* Tue Sep 29 2015 Lenz Grimmer <lenz@openattic.org> 2.0.3
- Fixed dependencies and moved preinstall section that creates the openattic
  user/group to the base subpackage (OP-536)
- Moved log files into /var/log/openattic, removed superflouous chown
  in the preinstall
- Replaced some legacy "system" calls with "systemctl"

* Mon Sep 07 2015 Lenz Grimmer <lenz@openattic.org> 2.0.2
- Updated package descriptions (fixed formatting)
- Added openattic-module-ceph subpackage (OP-624)
- Use a versioned tar ball as the build source
- Don't install bower and grunt as part of the RPM build process
- Reworked install section, use more RPM macros
- Removed compiled Python objects from the file list
- Added /var/lib/nagios3 to the nagios subpackage
- Added policycoreutils-python dependency to the gui package (OP-571)

* Thu May 21 2015 Michael Ziegler <michael@open-attic.org>
- Remove prep stuff
- Replace fixed version numbers by BUILDVERSION and PKGVERSION macros which are populated with info from HG
- rm the docs from RPM_BUILD_ROOT and properly install them through %doc

* Tue Feb 24 2015 Markus Koch  <mkoch@redhat.com> - 1.2 build
- split into package modules

* Fri May 23 2003 Markus Koch  <mkoch@redhat.com> - 1.2 build version 1
- First build.

