#  Copyright (C) 2011-2016, it-novum GmbH <community@openattic.org>
#
#  openATTIC is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; version 2.
#
#  This package is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

PYTHON="/usr/bin/python"
OADIR="/usr/share/openattic"
OAUSER="openattic"
OAGROUP="openattic"

SYSD_PIDFILE="/var/run/openattic_systemd.pid"
SYSD_LOGFILE="/var/log/openattic/openattic_systemd.log"
SYSD_LOGLEVEL="INFO"
SYSD_OPTIONS="$OADIR/manage.py runsystemd"

WEBSERVER_SERVICE="httpd"

## Type: string
## Default: "/var/spool/nagios/cmd/nagios.cmd"
#
# Path for processing commands submitted by other applications
NAGIOS_CMD_PATH="/var/spool/nagios/cmd/nagios.cmd"

## Type: string
## Default: "/var/spool/nagios/status.dat"
#
# The file that stores the current status, comment, and downtime information.
NAGIOS_STATUS_DAT_PATH="/var/spool/nagios/status.dat"
NAGIOS_STATUS_DAT="/var/spool/nagios/status.dat"

## Type: string
## Default: "/etc/nagios/nagios.cfg"
#
# The main configuration file
NAGIOS_CFG_PATH="/etc/nagios/nagios.cfg"
NAGIOS_CFG="/etc/nagios/nagios.cfg"

## Type: string
## Default: "/etc/nagios/conf.d"
#
# Defines additional services
NAGIOS_SERVICES_CFG_PATH="/etc/nagios/conf.d"

## Type: string
## Default: "nagios"
#
# Name of monitoring executable
NAGIOS_BINARY_NAME="nagios"

## Type: string
## Default: "nagios"
#
# Name of systemd service
NAGIOS_SERVICE_NAME="nagios"
NAGIOS_SERVICE="nagios"

## Type: string
## Default: "/usr/lib64/npcdmod.o"
#
# Path to the npcd module
NPCD_MOD="/usr/lib64/nagios/brokers/npcdmod.o"

## Type: string
## Default: "/etc/pnp4nagios/npcd.cfg"
#
# Configuration for npcd
NPCD_CFG="/etc/pnp4nagios/npcd.cfg"

## Type: string
## Default: "npcd"
#
# Name of systemd service
NPCD_SERVICE="npcd"

## Type: string
## Default: "/var/lib/pnp4nagios"
#
# Path to RRDs
NAGIOS_RRD_BASEDIR="/var/lib/pnp4nagios"

## Type: string
## Default: "/var/log/nagios"
#
# Directory in which to save states
NAGIOS_STATE_DIR="/var/log/nagios"

## Type: string
## Default: "/usr/lib64/nagios/plugins"
#
# Location of the Nagios plugin directory
NAGIOS_PLUGIN_DIR="/usr/lib64/nagios/plugins"
