��          �       �      �     �  *   �  ;   �  (   �  ;   %  B   a  2   �  -   �  ,     '   2  )   Z  (   �     �     �  S   �  (   =     f     ~      �     �     �  	   �  y  �     G  4   O  L   �  1   �  J     R   N  D   �  4   �  7     /   S  6   �  &   �      �  %   	  Y   (	  0   �	  !   �	     �	  #   �	     
     
  	   .
   802.3ad Bonding %s has ITSELF as one of its slaves Bonding %s has slaves with mismatching Jumbo Frames setting Bridge %s has ITSELF as one of its ports If this interface is VLAN device, name the raw device here. If this interface is a bonding device, add the slave devices here. If this interface is a bridge, add the ports here. Interface %s has an address without a netmask Interface %s has children and has an address Interface %s has children and uses DHCP Interface %s uses DHCP but has an address There is are no name servers configured. There is no default gateway. There is no domain configured. There is no interface that has an IP (none with dhcp and none with static address). Vlan %s has ITSELF as its base interface active-backup: Failover balance-alb balance-rr: Balanced Round Robin balance-tlb balance-xor broadcast Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-24 15:49+0100
PO-Revision-Date: 2014-02-24 16:17
Last-Translator:   <>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 802.3ad Bonding %s nutzt sich selbst als einen seiner Slaves Bonding %s benutzt andere Jumbo-Frames-Einstellungen als seine Basisgeräte. Bridge %s nutzt sich selbst als einen ihrer Ports Wenn dieses Gerät ein VLAN-Gerät ist, geben Sie hier das Basisgerät an. Wenn dieses Gerät ein Bonding-Gerät ist, fügen Sie hier die Basisgeräte hinzu. Wenn dieses Gerät eine Bridge ist, fügen Sie hier die Ports hinzu. Gerät %s hat eine Adresse ohne angegebene Netzmaske Interface %s hat abhängige Geräte und eine IP-Adresse Gerät %s hat abhängige Geräte und nutzt DHCP Gerät %s nutzt DHCP und hat eine statische IP-Adresse Es sind keine Nameserver konfiguriert. Es gibt keinen Standard-Gateway. Es ist kein Domain-Name konfiguriert. Kein Gerät hat eine IP-Adresse (keines nutzt DHCP und keines ist statisch konfiguriert). VLAN-Gerät %s nutzt sich selbst als Basisgerät Ausfallsicherheit (Active-Backup) balance-alb balance-rr: Balanciert: Round-Robin balance-tlb balance-xor: Balanciert: XOR Broadcast 