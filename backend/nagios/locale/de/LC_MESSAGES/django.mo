��          <      \       p   e   q      �   
   �   �  �   �   �     >  	   F                   Check this if openATTIC should not configure services with this command, only query those that exist. End time Start time Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-06-12 13:26+0200
PO-Revision-Date: 2012-01-18 12:26
Last-Translator: Michael Ziegler <michael.ziegler@it-novum.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.0
 Wählen Sie diese Option wenn openATTIC dieses Kommando nicht für neu konfigurierte Services benutzen soll, sondern nur diejenigen Services abfragen, die bereits existieren. Endzeit Startzeit 